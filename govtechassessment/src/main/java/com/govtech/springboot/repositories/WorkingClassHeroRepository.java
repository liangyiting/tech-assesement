package com.govtech.springboot.repositories;

import com.govtech.springboot.model.WorkingClassHero;
import org.springframework.data.jpa.repository.JpaRepository;


public interface WorkingClassHeroRepository extends JpaRepository<WorkingClassHero,Integer> {

}
