package com.govtech.springboot.utils;

import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvParser;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class CsvHelper {

    private static final CsvMapper mapper = new CsvMapper();
    public static <T> List<T> readWorkingClassHeroUpload(Class<T> clazz, InputStream stream) throws IOException {
        mapper.findAndRegisterModules();
        CsvSchema schema = CsvSchema.builder()
                .addColumn("natid")
                .addColumn("name")
                .addColumn("gender")
                .addColumn("salary")
                .addColumn("birthday")
                .addColumn("tax")
                .setUseHeader(true)
                .setStrictHeaders(true)
                .build();
        ObjectReader reader = mapper.readerFor(clazz).with(schema).with(CsvParser.Feature.SKIP_EMPTY_LINES);
        return reader.<T>readValues(stream).readAll();
    }
}
