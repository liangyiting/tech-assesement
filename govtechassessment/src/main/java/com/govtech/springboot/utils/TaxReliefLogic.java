package com.govtech.springboot.utils;

import com.govtech.springboot.model.WorkingClassHero.Gender;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

public class TaxReliefLogic {


    public static long calculateTaxReliefRounded(double salary, double taxpaid, Gender gender, LocalDate birthday) {
        BigDecimal taxRelief = ((BigDecimal.valueOf(salary).subtract(BigDecimal.valueOf(taxpaid)))
                .multiply(getVariable(birthday)))
                .add(getGenderBonus(gender));
        long taxReliefRounded = taxRelief.setScale(2, BigDecimal.ROUND_FLOOR)
                .setScale(0,BigDecimal.ROUND_HALF_UP)
                .longValue();
        if (taxReliefRounded > 0 && taxReliefRounded < 50) {
            return 50;
        }
        return taxReliefRounded;
    }

    private static BigDecimal getVariable(LocalDate birthday) {
        int age = getAge(birthday);
        if (age <= 18) {
            return new BigDecimal(1);
        } else if (age <= 35) {
            return new BigDecimal(0.8);
        } else if (age <= 50) {
            return new BigDecimal(0.5);
        } else if (age <= 75) {
            return new BigDecimal(0.367);
        } else {
            return new BigDecimal(0.05);
        }
    }

    private static int getAge(LocalDate birthday) {
        return Period.between(birthday, LocalDate.now()).getYears();
    }

    private static BigDecimal getGenderBonus(Gender gender) {
        if (gender==Gender.F) {
            return new BigDecimal(500);
        } else if (gender==Gender.M) {
            return new BigDecimal(0);
        }
        throw new IllegalArgumentException("Unexpected gender type - " + gender.name());
    }
}
