package com.govtech.springboot.controller;


import com.govtech.springboot.model.WorkingClassHero;
import com.govtech.springboot.repositories.WorkingClassHeroRepository;
import com.govtech.springboot.utils.CsvHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class WorkingClassHeroController {

    @Autowired
    WorkingClassHeroRepository repository;

    @PostMapping("/workingclasshero")
    public ResponseEntity<WorkingClassHero> addWorkingClassHero(@Valid @RequestBody WorkingClassHero newWorkingClassHero) {
        WorkingClassHero workingClassHero = repository.save(newWorkingClassHero);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(workingClassHero.getId()).toUri();
        return ResponseEntity.created(location).body(workingClassHero);
    }

    @PostMapping("/workingclasshero-collection")
    public List<ResponseEntity<WorkingClassHero>> addWorkingClassHeroCollection(@Valid @RequestBody List<WorkingClassHero> newWorkingClassHeroCollection) {
        WorkingClassHero workingClassHero;
        List<ResponseEntity<WorkingClassHero>> responseEntitiesList = new ArrayList<>();
        for (WorkingClassHero newWorkingClassHero : newWorkingClassHeroCollection) {
            workingClassHero = repository.save(newWorkingClassHero);
            URI location = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/workingclasshero/{id}").buildAndExpand(workingClassHero.getId()).toUri();
            responseEntitiesList.add(ResponseEntity.created(location).body(newWorkingClassHero));
        }
        return responseEntitiesList;
    }


    @GetMapping("/workingclasshero")
    public List<WorkingClassHero> retrieveWorkingClassHeroes() {
        return repository.findAll();
    }

    @GetMapping("/workingclasshero/{id}")
    public WorkingClassHero retrieveWorkingClassHeroes(@PathVariable Integer id) {
        Optional<WorkingClassHero> hero = repository.findById(id);
        if (!hero.isPresent()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Id not found : " + id);
        }
        return repository.findById(id).get();
    }

    @PostMapping(value = "/workingclasshero/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public List<WorkingClassHero> uploadCsv(@RequestParam("file") MultipartFile file) throws IOException {
        return repository.saveAll(CsvHelper.readWorkingClassHeroUpload(WorkingClassHero.class, file.getInputStream()));
    }

    @GetMapping(value = "/cashdispense")
    public String cashDispense() {
        return "Cash dispensed";
    }

}
