package com.govtech.springboot.model;

import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.govtech.springboot.utils.TaxReliefLogic;
import org.springframework.format.annotation.DateTimeFormat;
import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.LocalDate;

@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorkingClassHero {

    @Id
    @GeneratedValue
    private Integer id;

    @NotNull
    private String natid;

    @NotNull
    private String name;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Enumerated(EnumType.STRING)
    private Gender gender;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private double salary;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @Past
    @DateTimeFormat(pattern = "YYYY-MM-DD")
    private LocalDate birthday;

    @NotNull
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private double tax;

    @Transient
    @JsonGetter(value = "taxrelief")
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public long getTaxReflief() {
        return TaxReliefLogic.calculateTaxReliefRounded(salary, tax, gender, birthday);
    }

    public enum Gender {
        F, M
    }

    protected WorkingClassHero() {

    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNatid() {
        String maskedNatId = natid.substring(0, 5);
        for (int i = 0; i < natid.length() - 5; i++) {
            maskedNatId = maskedNatId + '$';
        }
        return maskedNatId;
    }

    public void setNatid(String natid) {
        this.natid = natid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public double getTax() {
        return tax;
    }

    public void setTax(double tax) {
        this.tax = tax;
    }

}
