# tech-assesement


Deployed URL on AWS : http://yiting-tech-assessement.us-east-2.elasticbeanstalk.com/

Auto generated Swagger API Doc : http://yiting-tech-assessement.us-east-2.elasticbeanstalk.com/swagger-ui/api-docs.html


To test the code, you can either deploy the app locally or visit the app hosted on AWS [here](http://yiting-tech-assessement.us-east-2.elasticbeanstalk.com/)


Below are some additional decsription on the various api calls created based on the high level user stories below. (non ui components)


---
### (1) As the Clerk, I should be able to insert a single record of working class hero into database via an API

The following POST api call will create one working class hero.

`POST : http://yiting-tech-assessement.us-east-2.elasticbeanstalk.com/workingclasshero `

Json Body Example :

```json
{
        "natid": "S1234567A",
        "name": "John",
        "gender": "M",
        "birthday": "2000-12-12",
        "salary": 8000,
        "taxpaid": 245
}

```
---

### (2) As the Clerk, I should be able to insert more than one working class hero into database via an API 

The following POST api call can create more than one working class hero.

`POST : http://yiting-tech-assessement.us-east-2.elasticbeanstalk.com/workingclasshero-collection/`

Request Json Body Example :

```json
[{
        "natid": "S0234567A",
        "name": "JohnA",
        "gender": "M",
        "birthday": "2000-12-12",
        "salary": 80000,
        "tax": 2450
},
{
        "natid": "S8247564B",
        "name": "MaryA",
        "gender": "F",
        "birthday": "1980-12-12",
        "salary": 20000,
        "tax": 1000
},
{
        "natid": "S8247564B",
        "name": "MaryB",
        "gender": "M",
        "birthday": "1980-12-12",
        "salary": 20000,
        "tax": 1000
},
{
        "natid": "S8247564B",
        "name": "MaryC",
        "gender": "F",
        "birthday": "1980-12-12",
        "salary": 50000,
        "tax": 1000
}]
```
---

### (3) As the Clerk, I should be able to upload a csv file to a portal so that I can populate the database from a UI 

`POST : http://yiting-tech-assessement.us-east-2.elasticbeanstalk.com/upload`

---

### (4) As the Bookkeeper, I should be able to query the amount of tax relief for each person in the database so that I can report the figures to my Bookkeeping Manager 

The following GET api call will retrieve one record of working class hero based on given id.

`GET http://yiting-tech-assessement.us-east-2.elasticbeanstalk.com/workingclasshero/{id}`

The following GET api call will retrieve all records of working class hero in the DB.

`GET : http://yiting-tech-assessement.us-east-2.elasticbeanstalk.com/workingclasshero`

